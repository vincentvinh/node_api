//Creation du serveur
var http = require('http');
var serv = require('./app.js');
//Reccuperation de l'URL saisie par Client
var url = require('url');

//Reccuperation des parametres de l'URL (complet)
var querystring = require('querystring');

//REQ = From Client | RES = To Client
var server = http.createServer(function(req, res) {
    //Print URL asked by client
    var page = url.parse(req.url).pathname;
    console.log(page);
    //Separation des parametres de l'URL
    var params = querystring.parse(url.parse(req.url).query);

    res.writeHead(200, {"Content-Type": "text/plain"});
    if ('prenom' in params && 'nom' in params) {
        res.write('Vous vous appelez ' + params['prenom'] + ' ' + params['nom']);
    }
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    if(page == '/') {

        res.write('Vous êtes à l\'accueil, que puis-je pour vous ?');

    }
    else if(page =='/stop'){
        server.close();
    }
    res.end();
});

server.on('close', function() { // On écoute l'évènement close
    console.log('Deconnection du serveur');
});

/* var EventEmitter = require('events').EventEmitter;

var jeu = new EventEmitter();

jeu.on('gameover', function(message){
    console.log(message);
});

jeu.emit('gameover', 'Vous avez perdu !'); */

server.listen(8080);
